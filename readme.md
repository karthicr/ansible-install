# Installing Ansible

Installs ansible on a new machine to run playbooks.

## Usage & Supported Platforms

### Ubuntu

Run the following:

```
sudo ./install_ubuntu
```
